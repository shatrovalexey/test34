<?php
	/**
	* Задание 3. Напишите PHP скрипт для подсчета белых пикселей на изображении.
	* Задачи:
	* 1. Сделайте форму для аплоада изображений.
	* 2. С помощью скрипта подсчитайте количество белых пикселей на загруженном изображении.
	* 3.  Отобразите результат на экране.
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/

	ini_set( 'display_errors' , E_ALL ) ;

	try {
		if ( empty( $_FILES[ 'file' ] ) ) {
			throw new Exception( 'Файл не указан' ) ;
		}

		if ( ! empty( $_FILES[ 'file' ][ 'error' ] ) ) {
			throw new Exception( 'Ошибка загрузки' ) ;
		}

		/**
		* @var $file_name string - путь к временному файлу
		*/

		$file_name = tempnam( '/tmp' , 'img' ) ;

		if ( ! move_uploaded_file( $_FILES[ 'file' ][ 'tmp_name' ] , $file_name ) ) {
			throw new Exception( 'Внутренняя ошибка' ) ;
		}

		/**
		* @var $ext string - расширение загруженного файла
		*/

		$ext = strToLower( pathinfo( $_FILES[ 'file' ][ 'name' ] , PATHINFO_EXTENSION ) ) ;

		/**
		* @var $sub_name string - имя функции для загрузки изображения для работы с ним
		*/

		switch ( $ext ) {
			case 'gif' : {
				$sub_name = 'imagecreatefromgif' ;

				break ;
			}
			case 'jpeg' :
			case 'jpg' : {
				$sub_name = 'imagecreatefromjpeg' ;

				break ;
			}
			case 'png' : {
				$sub_name = 'imagecreatefrompng' ;

				break ;
			}
			default:
				throw new Exception( 'Неизвестный тип файла' ) ;
		}

		/**
		* @var $image resource - загруженное изображение
		* @var $image_sx int - размер изображения по горизонтали
		* @var $image_sy int - размер изображения по вертикали
		* @var $x int - номер текущего столбца пикселей
		* @var $y int - номер текущей строки пикселей
		* @var $result int - количество белых пикселей
		* @const WHITE_COLOR int - код белого цвета
		*/

		$image = $sub_name( $file_name ) ;
		$image_sx = imagesx( $image ) ;
		$image_sx = imagesy( $image ) ;
		$result = 0 ;

		define( 'WHITE_COLOR' , 0xffffff ) ;

		for ( $x = 0 ; $x < $image_sx ; $x ++ ) {
			for ( $y = 0 ; $y < $image_sy ; $y ++ ) {
				if ( imagecolorat( $image , $x , $y ) != WHITE_COLOR ) {
					continue ;
				}

				$result ++ ;
			}
		}

		throw new Exception( 'Количество пикселей белого цвета: ' . $result ) ;
	} catch ( \Exception $exception ) {
		if ( ! empty( $image ) ) {
			imagedestroy( $image ) ;
		}
		if ( ! empty( $file_name ) ) {
			unlink( $file_name ) ;
		}

		echo $exception->getMessage( ) ;
	}