/**
Есть таблица users, в которой хранится информация о клиентах магазина. Необходимо с помощью SQL запроса, найти всех клиентов у которых скоро будет день рождения.

Условия:
1). Задача должна быть решена одним SQL запросом.
2). Должен получиться список клиентов, у которых день рождения будет в ближайшие 30 дней.

@author Shatrov Aleksej <mail@ashatrov.ru>
*/

SELECT
    `u1`.`id`
FROM
	`users` AS `u1`
WHERE
	( dayofyear( `u1`.`birthday` ) BETWEEN dayofyear( current_date( ) ) AND dayofyear( current_date( ) + INTERVAL 30 DAY ) )
ORDER BY
	dayofyear( `u1`.`birthday` ) DESC ;